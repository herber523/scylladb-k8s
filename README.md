# Scylla cluster with kubernetes


Scylladb cluster with Kubernetes

Using Kubernetes DaemonSet to setup a scylladb cluster.

## DaemontSet template of Scylladb cluster

```
apiVersion: extensions/v1beta1
kind: DaemonSet
metadata:
  name: scylladb
spec:
  template:
    metadata:
      labels:
        app: scylladb
    spec:
      containers:
      - name: scylladb
        image: registry.gitlab.com/herber523/scylladb-k8s
        args: ["--seeds", "auto"]
```

## Service template of Scylladb cluster

```
apiVersion: v1
kind: Service
metadata:
  labels:
    name: scylladb
  name: scylladb
spec:
  clusterIP: None
  ports:
  - name: cql
    port: 9042
    protocol: TCP
    targetPort: 9042
  selector:
    app: scylladb
```


## Play with it

Start the headless service
```
kubectl apply -f k8s/service.yaml
```

Start the scylladb cluster
```
kubectl apply -f k8s/daemonset.yaml
```